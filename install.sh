# On rentre dans le dossier de l'API
cd api

# Installation des dépendances
npm install
npm install express --save
npm install --save sequelize@latest
npm install mysql2
npm install bcrypt --save
npm install jsonwebtoken --save
npm install body-parser --save

# On rentre dans le dossier du Front
cd ../front

# Installation des dépendances
npm install
npm install express --save
npm install solid-js
npm run build
