import { Component, createSignal, Show} from "solid-js";
import Flex from "./layouts/Flex";
import Box from "./layouts/Box";
import { isConnected } from "./Session";
import { logout } from "./forms/ConnectForm";
import { useNavigate } from "@solidjs/router";
import LinkItems from "./LinkItems";
import "./css/Footer.css"

export const [connected, setConnected] = createSignal(isConnected())

const Footer: Component = () => {
    const nav = useNavigate()

    return (
        <Box bgc="#000000" h="300px" tp="0%" w="100%" p="0">
            <Flex jc="space-evenly" w="100%" m="0" p="0" h='100%' ai="center" td="none" ff="Roboto">
                <Flex w="20%" h="50%" c="#FFFFFF" direction="column" ta="center" ff="Roboto"> 
                    <h3>Suivez nous !</h3>
                    <Flex w="100%" h="85%" jc="space-evenly" ai="center">
                        <a href="https://www.tripadvisor.com" target="blank">
                            <img class="logo_reseau" src="/src/img/tripadvisor.png" alt="logo_tripadvisor" height="50px" />
                        </a>
                        <a href="https://www.facebook.com/" target="blank">
                            <img class="logo_reseau" src="/src/img/facebook.png" alt="logo_facebook" height="50px" />
                        </a>
                        <a href="https://www.instagram.com/" target="blank">
                            <img class="logo_reseau" src="/src/img/instagram.png" alt="logo_instagram" height="50px" />
                        </a>
                    </Flex>
                </Flex>
                <iframe class="maps_api" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d126931.62429646617!2d1.1642894989329937!3d6.1824860267098325!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x1023e1c113185419%3A0x3224b5422caf411d!2zTG9tw6k!5e0!3m2!1sfr!2stg!4v1690486368649!5m2!1sfr!2stg"></iframe>
                <p style="color: #FFF; font-size: 20px">TogoEat &copy tous droits réservés</p>
            </Flex>
        </Box>
    );
};


export default Footer;