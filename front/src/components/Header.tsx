import { Component, createSignal, Show} from "solid-js";
import Flex from "./layouts/Flex";
import Box from "./layouts/Box";
import "./css/Header.css";
import { isConnected } from "./Session";
import { logout } from "./forms/ConnectForm";
import { useNavigate } from "@solidjs/router";
import LinkItems from "./LinkItems";

export const [connected, setConnected] = createSignal(isConnected())

const Header: Component = () => {
    const nav = useNavigate()

    return (
        <Box bgc="#000000" h="140px" m="0" p="0">
            <Flex jc="space-evenly" w="100%" m="0" p="0" h="100%" ai="center" td="none" ff="Roboto">
                <img class="logo" src="/src/img/logo.png" alt="logo" height="100px" onClick={() => {nav("/", {replace: true})}}/>
                <ul><LinkItems path="/" text={"Accueil"}/></ul>
                <ul><LinkItems path="/plats" text={"Plats"}/></ul>
                <ul><LinkItems path="/boissons" text={"Boissons"}/></ul>
                <ul><LinkItems path="/dashboard" text={"Dashboard"}/></ul>
                <ul><LinkItems path="/contact" text={"Contact"}/></ul>
                <ul><Show when={connected()}><button class="logout" onclick={() => {logout(); setConnected(false); nav("/redirect", { replace: true }); }}>Logout</button></Show></ul>
            </Flex>
        </Box>
    );
};

export default Header;