import { Component} from 'solid-js';
import Home from './pages/Home'

import {Route, Routes } from '@solidjs/router';
import Header from './components/Header';
import Register from './pages/Register';
import Redirect from './pages/Redirect';
import Boissons from './pages/Boissons';
import Error404 from './pages/404';
import Plats from './pages/Plats';
import Connect from './pages/Connect'
import Dashboard from './pages/Dashboard';
import Contact from './pages/Contact';

const App: Component = () => {
	return (
		<div>
			<Header/>
			<Routes>
				<Route path="/" element={<Home/>}/>
				<Route path="/connect" element={<Connect/>}/>
				<Route path="/register" element={<Register/>}/>
				<Route path="/redirect" element={<Redirect/>} />
				<Route path="/plats" element={<Plats/>} />
				<Route path="/boissons" element={<Boissons/>} />
				<Route path="/dashboard" element={<Dashboard/>} />
				<Route path="/contact" element={<Contact/>} />
				<Route path="*" element={<Error404/>}/>
			</Routes>
		</div>
	);
};

export default App;
