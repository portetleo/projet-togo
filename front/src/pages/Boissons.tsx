import { Component, createSignal, For, onMount } from "solid-js";
import { useNavigate } from "@solidjs/router";
import Flex from "../components/layouts/Flex";
import Box from "../components/layouts/Box";
import "./css/Boissons.css";

interface Boisson {
  id: number;
  name: string;
  price: number;
  description: string;
  image: string;
  category: string;
}

const Boissons: Component = () => {
  const [boissons] = createSignal<Boisson[]>([
    {
      id: 1,
      name: "Boisson 1",
      price: 2.99,
      description: "Description de la boisson 1",
      image: "/src/img/boisson1.jpg",
      category: "vin",
    },
    {
      id: 2,
      name: "Boisson 2",
      price: 3.99,
      description: "Description de la boisson 2",
      image: "/src/img/boisson2.jpg",
      category: "vin",
    },
    {
      id: 3,
      name: "Boisson 3",
      price: 4.99,
      description: "Description de la boisson 3",
      image: "/src/img/boisson3.jpg",
      category: "vin",
    },
    {
      id: 4,
      name: "Boisson 4",
      price: 2.49,
      description: "Description de la boisson 4",
      image: "/src/img/boisson4.jpg",
      category: "vin",
    },
    {
      id: 5,
      name: "Boisson 1",
      price: 2.99,
      description: "Description de la boisson 1",
      image: "/src/img/boisson2.jpg",
      category: "alcool",
    },
    {
      id: 6,
      name: "Boisson 2",
      price: 3.99,
      description: "Description de la boisson 2",
      image: "/src/img/boisson3.jpg",
      category: "alcool",
    },
    {
      id: 7,
      name: "Boisson 1",
      price: 4.99,
      description: "Description de la boisson 1",
      image: "/src/img/boisson1.jpg",
      category: "soft",
    },
    {
      id: 8,
      name: "Boisson 2",
      price: 2.49,
      description: "Description de la boisson 2",
      image: "/src/img/boisson4.jpg",
      category: "soft",
    },
  ]);

  const handleCategoryClick = (category: string) => {
    const sectionId = category.toLowerCase();
    const section = document.getElementById(sectionId);
    if (section) {
      section.scrollIntoView({ behavior: "smooth" });
    }
  };

  onMount(() => {
    window.scrollTo(0, 0);
  });

  return (
    <Flex bgc="#FFFFFF" direction="column" w="100%" h="100%">
      <Flex h="20%" w="100%" fsz="32px" c="#000000">
        <h1 class="text-title">Boissons</h1>
      </Flex>

      <Flex direction="row" w="100%" h="90%">
        {/* Menu */}
        <Flex direction="column" pos="fixed" w="15%" jc="start" ai="center" ff="Roboto">
          <h2>Menu</h2>
          <li>
            <button onClick={() => handleCategoryClick("soft")}>Soft</button>
          </li>
          <li>
            <button onClick={() => handleCategoryClick("alcool")}>Alcool</button>
          </li>
          <li>
            <button onClick={() => handleCategoryClick("cocktail")}>Cocktail</button>
          </li>
          <li>
            <button onClick={() => handleCategoryClick("vin")}>Vin</button>
          </li>
        </Flex>

        <Flex direction="column" w="85%" ml="15%">
          {/* Soft */}
          <Box p="20px" ta="center" ff="Roboto">
            <h1 id="soft">Softs</h1>
            <Flex fw="wrap" jc="space-around" ai="center">
              <For each={boissons()}>
                {(boisson) => {
                  if (boisson.category === "soft") {
                    return (
                      <Flex direction="row" w="45%" mb="20px" jc="space-evenly" ai="center" bgc="#cef2e3" br="10px" pt="25px" pb="25px">
                        <img class="items_boisson" src={boisson.image} alt={boisson.name}/>
                        <Flex direction="column" ta="left" ff="Roboto">
                          <h3>{boisson.name}</h3>
                          <p>{boisson.description}</p>
                          <p>Prix : {boisson.price} €</p>
                        </Flex>
                      </Flex>
                    );
                  }
                  return null;
                }}
              </For>
            </Flex>
          </Box>

          {/* Alcool */}
          <Box p="20px" ta="center" ff="Roboto">
            <h1 id="alcool">Alcools</h1>
            <Flex fw="wrap" jc="space-around" ai="center">
              <For each={boissons()}>
                {(boisson) => {
                  if (boisson.category === "alcool") {
                    return (
                      <Flex direction="row" w="45%" mb="20px" jc="space-evenly" ai="center" bgc="#cef2e3" br="10px" pt="25px" pb="25px">
                        <img class="items_boisson" src={boisson.image} alt={boisson.name}/>
                        <Flex direction="column" ta="left" ff="Roboto">
                          <h3>{boisson.name}</h3>
                          <p>{boisson.description}</p>
                          <p>Prix : {boisson.price} €</p>
                        </Flex>
                      </Flex>
                    );
                  }
                  return null;
                }}
              </For>
            </Flex>
          </Box>

          {/* Cocktail */}
          <Box p="20px" ta="center" ff="Roboto">
            <h1 id="cocktail">Cocktails</h1>
            <Flex fw="wrap" jc="space-around" ai="center">
              <For each={boissons()}>
                {(boisson) => {
                  if (boisson.category === "cocktail") {
                    return (
                      <Flex direction="row" w="45%" mb="20px" jc="space-evenly" ai="center" bgc="#cef2e3" br="10px" pt="25px" pb="25px">
                        <img class="items_boisson" src={boisson.image} alt={boisson.name}/>
                        <Flex direction="column" ta="left" ff="Roboto">
                          <h3>{boisson.name}</h3>
                          <p>{boisson.description}</p>
                          <p>Prix : {boisson.price} €</p>
                        </Flex>
                      </Flex>
                    );
                  }
                  return null;
                }}
              </For>
            </Flex>
          </Box>

          {/* Vin */}
          <Box p="20px" ta="center" ff="Roboto">
            <h1 id="vin">Vins</h1>
            <Flex fw="wrap" jc="space-around" ai="center">
              <For each={boissons()}>
                {(boisson) => {
                  if (boisson.category === "vin") {
                    return (
                      <Flex direction="row" w="45%" mb="20px" jc="space-evenly" ai="center" bgc="#cef2e3" br="10px" pt="25px" pb="25px">
                        <img class="items_boisson" src={boisson.image} alt={boisson.name}/>
                        <Flex direction="column" ta="left" ff="Roboto">
                          <h3>{boisson.name}</h3>
                          <p>{boisson.description}</p>
                          <p>Prix : {boisson.price} €</p>
                        </Flex>
                      </Flex>
                    );
                  }
                  return null;
                }}
              </For>
            </Flex>
          </Box>
        </Flex>
      </Flex>
    </Flex>
  );
};

export default Boissons;
