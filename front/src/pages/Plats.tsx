import { Component, For, JSX, createSignal, onMount } from "solid-js";
import { useNavigate } from "@solidjs/router";
import Flex from "../components/layouts/Flex";
import Box from "../components/layouts/Box";
import "./css/Plats.css";

interface Plat {
  id: number;
  name: string;
  price: number;
  description: string;
  image: string;
  category: string;
}

const Plats: Component = () => {
  const [plats] = createSignal<Plat[]>([
    {
      id: 1,
      name: "Plat 1",
      price: 2.99,
      description: "Description du plat 1",
      image: "/src/img/plat1.jpg",
      category: "entree",
    },
    {
      id: 2,
      name: "Plat 2",
      price: 3.99,
      description: "Description du plat 2",
      image: "/src/img/plat2.jpg",
      category: "entree",
    },
    {
      id: 3,
      name: "Plat 3",
      price: 4.99,
      description: "Description du plat 3",
      image: "/src/img/plat3.jpg",
      category: "entree",
    },
    {
      id: 4,
      name: "Plat 4",
      price: 2.49,
      description: "Description du plat 4",
      image: "/src/img/plat4.jpg",
      category: "entree",
    },
    {
      id: 5,
      name: "Plat 1",
      price: 2.99,
      description: "Description du plat 1",
      image: "/src/img/plat1.jpg",
      category: "plat",
    },
    {
      id: 6,
      name: "Plat 2",
      price: 3.99,
      description: "Description du plat 2",
      image: "/src/img/plat3.jpg",
      category: "plat",
    },
    {
      id: 7,
      name: "Plat 3",
      price: 4.99,
      description: "Description du plat 3",
      image: "/src/img/plat2.jpg",
      category: "dessert",
    },
    {
      id: 8,
      name: "Plat 4",
      price: 2.49,
      description: "Description du plat 4",
      image: "/src/img/plat4.jpg",
      category: "dessert",
    },
  ]);

  const handleCategoryClick = (category: string) => {
    const sectionId = category.toLowerCase();
    const section = document.getElementById(sectionId);
    if (section) {
      section.scrollIntoView({ behavior: "smooth" });
    }
  };

  onMount(() => {
    window.scrollTo(0, 0);
  });

  return (
    <Flex bgc="#FFFFFF" direction="column" w="100%" h="100%">
      <Flex h="20%" w="100%" fsz="32px" c="#000000">
        <h1 class="text-title">Plats</h1>
      </Flex>

      <Flex direction="row" w="100%" h="90%">
        {/* Menu */}
        <Flex direction="column" pos="fixed" w="15%" jc="start" ai="center" ff="Roboto">
          <h2>Menu</h2>
          <li>
            <button onClick={() => handleCategoryClick("entree")}>Entrées</button>
          </li>
          <li>
            <button onClick={() => handleCategoryClick("plat")}>Plats</button>
          </li>
          <li>
          <button onClick={() => handleCategoryClick("dessert")}>Desserts</button>
          </li>
        </Flex>

        <Flex direction="column" w="85%" ml="15%">
          {/* Entrées */}
          <Box p="20px" ta="center" ff="Roboto">
            <h1 id="entree">Entrées</h1>
            <Flex fw="wrap" jc="space-around" ai="center">
              <For each={plats()}>
                {(plat) => {
                  if (plat.category === "entree") {
                    return (
                      <Flex direction="row" w="45%" mb="20px" jc="space-evenly" ai="center" bgc="#cef2e3" br="10px" pt="25px" pb="25px">
                        <img class="items_plat" src={plat.image} alt={plat.name}/>
                        <Flex direction="column" ta="left" ff="Roboto">
                          <h3>{plat.name}</h3>
                          <p>{plat.description}</p>
                          <p>Prix : {plat.price} €</p>
                        </Flex>
                      </Flex>
                    );
                  }
                return null;
                }}
              </For>
            </Flex>
          </Box>

          {/* Plats */}
          <Box p="20px" ta="center" ff="Roboto">
            <h1 id="plat">Plats</h1>
            <Flex fw="wrap" jc="space-around" ai="center">
              <For each={plats()}>
                  {(plat) => {
                    if (plat.category === "plat") {
                      return (
                        <Flex direction="row" w="45%" mb="20px" jc="space-evenly" ai="center" bgc="#cef2e3" br="10px" pt="25px" pb="25px">
                          <img class="items_plat" src={plat.image} alt={plat.name}/>
                          <Flex direction="column" ta="left" ff="Roboto">
                            <h3>{plat.name}</h3>
                            <p>{plat.description}</p>
                            <p>Prix : {plat.price} €</p>
                          </Flex>
                        </Flex>
                      );
                  }
                  return null;
                }}
              </For>
            </Flex>
          </Box>

          {/* Desserts */}
          <Box p="20px" ta="center" ff="Roboto">
            <h1 id="dessert" >Desserts</h1>
            <Flex fw="wrap" jc="space-around" ai="center">
            <For each={plats()}>
                {(plat) => {
                  if (plat.category === "dessert") {
                    return (
                      <Flex direction="row" w="45%" mb="20px" jc="space-evenly" ai="center" bgc="#cef2e3" br="10px" pt="25px" pb="25px">
                        <img class="items_plat" src={plat.image} alt={plat.name}/>
                        <Flex direction="column" ta="left" ff="Roboto">
                          <h3>{plat.name}</h3>
                          <p>{plat.description}</p>
                          <p>Prix : {plat.price} €</p>
                        </Flex>
                      </Flex>
                    );
                  }
                  return null;
                }}
              </For>
            </Flex>
          </Box>
        </Flex>
      </Flex>
    </Flex>
  );
};

export default Plats;
