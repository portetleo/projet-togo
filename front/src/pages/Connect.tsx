import { Component, createEffect, createSignal } from "solid-js";
import Flex from "../components/layouts/Flex";
import ButtonCustom from "../components/generals/ButtonCustom";
import { submit, form, setForm } from "../components/forms/ConnectForm";
import InputCustom from "../components/generals/InputCustom";
import { useNavigate } from "@solidjs/router";
import { setConnected } from "../components/Header";

const [stat, setStat] = createSignal(true);

const Connect: Component = () => {
    const nav = useNavigate();

    const handle_submit = async (event: Event) => {
        event.preventDefault();
        const status = await submit(form);
        setStat(status); // Utilisez setStat pour mettre à jour la valeur du signal
        if (status) { // Utilisez status pour vérifier si l'opération est réussie
            setConnected(true);
            nav("/redirect", { replace: true });
        }
    };

    createEffect(() => {
        if (stat()) {
            (document.getElementById("form-invalid-identifiers") as HTMLInputElement).innerHTML = "";
        } else {
            (document.getElementById("form-invalid-identifiers") as HTMLInputElement).innerHTML = "Wrong identifiers";
        }
    });

    return (
        <Flex jc="center" ai="center" bg="#FFFFFF" w="100%" h="calc(100vh - 140px)" m="0" p="0">
            <Flex jc="space-evenly" ai="center" w="35%" h="70%" br="50px" m="2vh" bgc="#3E3E3E" opt="90%" p="1em 3em" c="white" direction="column" ff="Roboto">
                <h1>Connexion</h1>
                <span id="form-invalid-identifiers"></span>
                <form onSubmit={ handle_submit }>
                    <Flex direction="row">
                        <Flex direction="column" jc="space-evenly" ai="center" w="100%">
                            <Flex direction="column" mb="15%" ff="Roboto" jc="center" ai="center">
                                <InputCustom ff="Roboto" id="email" label="E-mail" type="email" placeholder="E-mail" update={setForm}/>
                            </Flex>
                            <Flex direction="column" mb="15%" ff="Roboto">
                                <InputCustom ff="Roboto" id="pwd" label="Mot de passe" type="password" placeholder="Mot de passe" update={setForm}/>
                            </Flex>
                        </Flex>
                    </Flex>
                    <Flex jc="center" ai="center" ff="Roboto">
                        <ButtonCustom bgc="#054439" ff="Roboto" w="23.3em" h="4.4em" class="form-submit" type="submit" value="submit" text="Valider" />
                    </Flex>
                </form>
            </Flex>
        </Flex>
    );
};

export default Connect;