import { Component, For, Show, createSignal, onMount, onCleanup } from "solid-js";
import Box from "../components/layouts/Box";
import Flex from "../components/layouts/Flex";
import "./css/Home.css"
import ButtonCustom from "../components/generals/ButtonCustom";
import { isConnected } from "../components/Session";
import { useNavigate } from "@solidjs/router";
import Footer from '../components/Footer';


const Home: Component = () => {
    const nav = useNavigate()

    return (
        <Box>
            <Box w="100%" h="calc(180vh - 140px)" m="0" p="0" ovy="hidden">
                <Flex c="#000000">
                    <h1 class="text-title">
                        <span>Bienvenue sur TogoEat</span>
                    </h1>
                </Flex>
                <Flex direction="row">
                    <Flex direction="column" mt="2%" ml="15%">
                        <Show when={!isConnected()}>
                            <ButtonCustom text="CONNEXION" ff="Roboto black" fsz="16px" w="183px" h="48px" br="16px" bgc="#579074" mb="10%" onclick={() => {nav("/connect", {replace: true})}}/>
                            <ButtonCustom text="INSCRIPTION" ff="Roboto black" fsz="16px" w="183px" h="48px" br="16px" bgc="#579074" mb="10%" onclick={() => {nav("/register", {replace: true})}}/>
                        </Show>
                    </Flex>
                    <Box c="#000000" w="37%" ml="30%" mt="2%" fsz="20px" ff="Roboto" ta="justify">
                        TogoEat est une application complète pour la gestion des bars et restaurants. 
                        Elle facilite la prise de commande, le suivi des commandes, ainsi que la gestion des dépenses, des recettes et du stock.
                        L'interface intuitive permet une prise de commande facile, tandis que le suivi en temps réel assure une coordination
                        fluide entre le personnel en salle et en cuisine. Les propriétaires peuvent également gérer les dépenses, enregistrer les
                        recettes, générer des rapports détaillés et analyser les performances pour prendre des décisions éclairées.
                        TogoEat garantit la sécurité des données et offre une accessibilité optimale.
                    </Box>
                </Flex>

                <Box mt="11%" w="100%" h="42%" ff="Roboto">
                    <div class="slider-wrapper flex">
                        <div class="slide flex">
                            <div class="slide-image slider-link prev"><img src="./src/img/boisson1.jpg"/><div class="overlay"></div></div>
                            <div class="slide-content">
                                <div class="slide-date">30.07.2017.</div>
                                <div class="slide-title">LOREM IPSUM DOLOR SITE MATE, AD EST ABHORREANT</div>
                                <div class="slide-text">Lorem ipsum dolor sit amet, ad est abhorreant efficiantur, vero oporteat apeirian in vel. Et appareat electram appellantur est. Ei nec duis invenire. Cu mel ipsum laoreet, per rebum omittam ex. </div>
                                <div class="slide-more">READ MORE</div>
                            </div>	
                        </div>
                        <div class="slide flex">
                            <div class="slide-image slider-link next"><img src="./src/img/plat2.jpg"/><div class="overlay"></div></div>
                            <div class="slide-content">
                                <div class="slide-date">30.08.2017.</div>
                                <div class="slide-title">LOREM IPSUM DOLOR SITE MATE, AD EST ABHORREANT</div>
                                <div class="slide-text">Lorem ipsum dolor sit amet, ad est abhorreant efficiantur, vero oporteat apeirian in vel. Et appareat electram appellantur est. Ei nec duis invenire. Cu mel ipsum laoreet, per rebum omittam ex. </div>
                                <div class="slide-more">READ MORE</div>
                            </div>	
                        </div>	
                        <div class="slide flex">
                            <div class="slide-image slider-link next"><img src="./src/img/boisson3.jpg"/><div class="overlay"></div></div>
                            <div class="slide-content">
                                <div class="slide-date">30.09.2017.</div>
                                <div class="slide-title">LOREM IPSUM DOLOR SITE MATE, AD EST ABHORREANT</div>
                                <div class="slide-text">Lorem ipsum dolor sit amet, ad est abhorreant efficiantur, vero oporteat apeirian in vel. Et appareat electram appellantur est. Ei nec duis invenire. Cu mel ipsum laoreet, per rebum omittam ex. </div>
                                <div class="slide-more">READ MORE</div>
                            </div>	
                        </div>
                            <div class="slide flex">
                            <div class="slide-image slider-link next"><img src="./src/img/plat4.jpg"/><div class="overlay"></div></div>
                            <div class="slide-content">
                                <div class="slide-date">30.10.2017.</div>
                                <div class="slide-title">LOREM IPSUM DOLOR SITE MATE, AD EST ABHORREANT</div>
                                <div class="slide-text">Lorem ipsum dolor sit amet, ad est abhorreant efficiantur, vero oporteat apeirian in vel. Et appareat electram appellantur est. Ei nec duis invenire. Cu mel ipsum laoreet, per rebum omittam ex. </div>
                                <div class="slide-more">READ MORE</div>
                            </div>	
                        </div>
                    </div>
                </Box>
            </Box>
            <Footer/>
        </Box>
    )
}

export default Home

function createEffect(arg0: () => void) {
        throw new Error("Function not implemented.");
    }
