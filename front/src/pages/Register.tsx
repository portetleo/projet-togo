import { Component, createEffect, createSignal, onMount } from "solid-js";
import Flex from "../components/layouts/Flex";
import ButtonCustom from "../components/generals/ButtonCustom";
import InputCustom from "../components/generals/InputCustom";
import './css/Register.css';
import { setStudentForm, studentForm, submit_student } from "../components/forms/RegisterStudentForm";


const Register: Component = () => {    
    return (
        <Flex ai="center" jc="space-evenly" w="100%" bg="#FFFFFF" h="calc(100vh - 140px)" direction="row">
            <Flex direction="column" jc="space-evenly" ai="center" w="50%" h="85%" br="50px" bgc="#3E3E3E" opt="90%" c="white">
                <h3 id="signup-title">Inscription</h3>
                <form id="form-register">
                    <Flex direction="row" jc="space-evenly" ai="center" w="100%" h="80%">
                        <Flex direction="column" jc="center" ai="center" w="45%" h="80%">
                            <Flex direction="column" w="98%">
                                <InputCustom id="name" label="Prénom" type="text" placeholder="Prénom" update={setStudentForm}/>
                            </Flex>
                            <Flex direction="column" w="98%" mt="8%">
                                <InputCustom id="email" label="E-mail" type="email" placeholder="E-mail" update={setStudentForm}/>
                            </Flex>
                            <Flex direction="column" w="98%" mt="8%">
                                <InputCustom id="telephone_number" label="Numéro de téléphone" type="tel" placeholder="Numéro de téléphone" update={setStudentForm} pattern="[0-9]{2}[0-9]{2}[0-9]{2}[0-9]{2}[0-9]{2}|[0-9]{2} [0-9]{2} [0-9]{2} [0-9]{2} [0-9]{2}|[0-9]{2}.[0-9]{2}.[0-9]{2}.[0-9]{2}.[0-9]{2}|\+33 [1-9] [0-9]{2} [0-9]{2} [0-9]{2} [0-9]{2}|\+33[1-9][0-9]{2}[0-9]{2}[0-9]{2}[0-9]{2}|[1-9][0-9]{2}[0-9]{2}[0-9]{2}[0-9]{2}|[1-9] [0-9]{2} [0-9]{2} [0-9]{2} [0-9]{2}|\+[0-9]{15}"></InputCustom>
                            </Flex>
                        </Flex>
                        <Flex direction="column" jc="center" ai="center" w="45%" h="80%" pt="19px">
                            <Flex direction="column" w="98%">
                                <InputCustom id="family_name" label="Nom" type="text" placeholder="Nom" update={setStudentForm}/>
                            </Flex>
                            <Flex direction="column" w="98%" mt="8%">
                                <InputCustom id="password" label="Mot de passe" type="password" placeholder="Mot de passe" update={setStudentForm}/>
                            </Flex>
                            <Flex direction="column" w="98%" mt="8%">
                                <InputCustom id="password_2" label="Confirmation mot de passe" type="password" placeholder="Confirmation" update={setStudentForm}/>
                            </Flex>
                        </Flex>
                    </Flex>
                    <span id="form-not-same-password-message"></span>
                    <Flex jc="center" ai="center">
                        <ButtonCustom id="form-submit" type="submit" value="submit" h="71px" w="373px" bgc="#054439" ff="Roboto" text="Valider" />
                    </Flex>
                </form>
            </Flex>
        </Flex>
    )
}

export default Register