import { Component, For, createSignal, onMount } from "solid-js"
import Flex from "../components/layouts/Flex"
import { useNavigate } from "@solidjs/router"
import ButtonCustom from "../components/generals/ButtonCustom";
import InputCustom from "../components/generals/InputCustom";
import Box from "../components/layouts/Box"
import "./css/Contact.css"
import Footer from '../components/Footer';

const Contact: Component = () => {
    return (
        <Box>
            <Flex w="100%" h="calc(100vh - 140px)" ovy="hidden" jc="center" ai="center">
                <Flex direction="row" jc="center" ai="center" w="60%" h="85%" bgc="#444444" br="10px">
                    <Flex jc="space-evenly" ai="center" c="white" w="80%" h="80%" direction="column" ff="Roboto"> 
                        <form class="form-contact">
                            <Flex direction="column" w="100%" h="70%" jc="space-evenly" ai="center" ff="Roboto">
                                <input id="object" type="text" required  placeholder="Object"/>
                                <textarea id="content" required placeholder="Content"/>
                            </Flex>
                            <Flex direction="column" w="100%" h="30%" jc="space-evenly" ai="center">
                                <Flex direction="row" w="80%" h="50%" jc="space-evenly" ai="center">
                                    <select class="select-form">
                                    </select>
                                    <ButtonCustom w="10%" ff="Roboto" type="submit" value="Send" text="Send"/>
                                </Flex>
                            </Flex>
                        </form>
                    </Flex>
                </Flex>
            </Flex>
            <Footer/>
        </Box>
    )
}

export default Contact

