import { Component, For, createSignal, onMount } from "solid-js"
import Flex from "../components/layouts/Flex"
import { useNavigate } from "@solidjs/router"
import Box from "../components/layouts/Box"

const Dashboard: Component = () => {

    return (
        <Flex bgc="#FFFFFF" direction="column" w="100%" h="120vh">
            <Flex h="20%" w="100%" fsz="56px" c="#000000">
                <h1 class="text-title">Dashbord</h1>
            </Flex>
            <Flex fw="wrap" direction="row" jc="space-evenly" w="100%" h="100%">
                {/* Faudra faire des calls à l'API + une boucle for each pour récupérer et afficher 
                    ce qu'on veut */}
            </Flex>
        </Flex>
    )
}

export default Dashboard