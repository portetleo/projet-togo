# TOGOEAT

## Description

Ce projet est une application Web permettant la gestion du bar ou d'un restaurant au Togo.

## Outils

- NodeJS
- NPM
- Sequelize
- MySQL
- ExpressJS
- SolidJS

## Installation

Il suffit de lancer le script install.sh en vérifiant que vous avez les droit :

```
chmod +x install.sh
```

```
./install.sh
```

## Creation de la BDD 

Il faut se connecter à mysql en utilisateur ```root``` et sans mot de passe. Ou modifier le fichier config.json si plus simple.

Une fois dans mysql, tapez la commande suivant : 
```
CREATE DATABASE togoeat_database;
```
Un fois ça fait vous pouvez quitter mysql et vous rendre dans le dossier ```/api``` puis exécuter la commande : 
```
sequelize db:migrate
```
ce qui exécutera tous les fichiers de migrations normalement sans erreurs si toutes les installations sont réalisées correctement.

## Lancement

Il suffit de lancer le script start.sh en vérifiant que vous avez les droit :

```
chmod +x start.sh
```

```
./start.sh
```

## Auteurs

- LESIEUR Esteban
- LOEVENBRUCK Pierre
- MULTON Mateo
- PORTET Léo
- SABADIE Laura