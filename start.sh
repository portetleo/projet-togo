#!/bin/bash

echo "Insertion dans la BDD"
cd api
node insert.js 

# Lancement du serveur
echo "Lancement de l'API"
npm start &

# Script de lancement du Front-end
cd ../front

echo "Lancement du Front-end"
npm run dev &
