// Imports
const express = require('express');
const bodyParser = require('body-parser');
const apiRouter = require('./apiRouter').router;
const Sequelize = require('sequelize');
const models = require('./models');
const cors = require('cors');
const path = require('path');

// Instantiate server
var server = express();

// Activez CORS pour toutes les routes
server.use(cors());

// Déplacer la définition des routes ici, avant la configuration du middleware
server.use('/api/', apiRouter);


// Body parser configuration
server.use(bodyParser.urlencoded({ extended: true }));
server.use(bodyParser.json());

// Définir le chemin du répertoire statique pour le frontend (dossier "front")
const frontEndPath = path.join(__dirname, '..', 'front', 'dist');
server.use(express.static(frontEndPath));

// Configure Sequelize
var sequelize = new Sequelize('togoeat_database', 'root', '', {
    host: 'localhost',
    dialect: 'mysql',
    // ... autres options de configuration de Sequelize
    port: 3306,
    logging: false,
});

// Initialize Sequelize and establish database connection
models.sequelize
    .authenticate()
    .then(() => {
        console.log('Connexion à la base de données établie avec succès.');

        // Start the server
        server.listen(8080, function () {
            console.log('[INFO] TOGOEAT API started on port 8080');
        });
    })
    .catch((error) => {
        console.error('Erreur lors de la connexion à la base de données:', error);
    });

// Configure routes
server.get('/', function (req, res) {
    res.redirect("/")
});

