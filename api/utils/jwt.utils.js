// Imports
var jwt = require('jsonwebtoken');

const JWT_SIGN_SECRET = '0sjs6gf9mk9nwzq22zelkndfmlezfd521qer2szeze9erazx0zfzf6cc'
// Exported functions
module.exports = {
    generateTokenForUser: function(userData) {
        return jwt.sign({
            email: userData.email
        },
        JWT_SIGN_SECRET,
        {
            expiresIn: '1h' // exprire in 1 hour
        })
    }
}