// Imports
const bcrypt = require('bcrypt');
const models = require('../models/index');
var jwtUtils = require('../utils/jwt.utils');

// Routes
module.exports = {
    register: function (req, res) {
        const { email, 
                first_name,
                family_name, 
                pwd, 
                phone,
                role
            } = req.body;

        if (email == null || pwd == null) {
            return res.status(400).json({ 'error': 'missing parameters' });
        }

        // Verify Params
        models.Users.findOne({
        where: { email: email },
        })
        .then(function (userFound) {
            if (!userFound) {
            bcrypt.hash(pwd, 5, function (err, hashedPassword) {
                models.Users.create({
                email: email,
                first_name: first_name,
                family_name: family_name,
                pwd: hashedPassword,
                phone: phone,
                role: role
                })
                .then(function (newUser) {
                    return res.status(201).json({
                        email: newUser.email,
                    });
                })
                .catch(function (err) {
                    return res.status(500).json({ error: 'Cannot add user' });
                });
            });
            } else {
            return res.status(409).json({ error: 'User already exists' });
            }
        })
        .catch(function (err) {
            return res.status(500).json({ error: 'Unable to verify user' });
        });
    },

    login: function (req, res) {
        // Params
        var email = req.body.email;
        var pwd = req.body.pwd;

        console.log("[DEBUG] Email:", email); // Ajout de logs
        console.log("[DEBUG] Password:", pwd); // Ajout de logs
    
        if (email == null || pwd == null) {
            console.log("[DEBUG] Missing parameters");
            return res.status(400).json({ 'error': 'missing parameters', email, pwd });
        }
    
        console.log("[DEBUG] models:", models); // Vérifiez si le modèle Users est bien inclus ici
    
        models.Users.findOne({
            attributes: ['email', 'pwd'], // Assurez-vous que 'pwd' est également sélectionné ici
            where: { email: email }
        })
        .then(function (userFound) {
            console.log("[DEBUG] User found in database:", userFound);
    
            if (userFound && userFound.email && userFound.pwd) { // Vérification des propriétés 'email' et 'pwd'
                bcrypt.compare(pwd, userFound.pwd, function (errBycrypt, resBycrypt) {
                    if (resBycrypt) {
                        return res.status(200).json({
                            'email': userFound.email,
                            'token': jwtUtils.generateTokenForUser(userFound)
                        });
                    } else {
                        return res.status(403).json({ 'error': 'invalid password' });
                    }
                });
            } else {
                return res.status(404).json({ 'error': 'this user doesn\'t exist' });
            }
        })
        .catch(function (err) {
            console.log("[ERROR] Error during login:", err);
            return res.status(500).json({ 'error': 'unable to verify user' });
        });
    },
    
};
