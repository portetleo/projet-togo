const { Sequelize } = require('sequelize');
const config = require('./config/config.json');
const bcrypt = require('bcrypt');

const sequelize = new Sequelize(config.development.database, config.development.username, config.development.password, {
    host: config.development.host,
    dialect: config.development.dialect
});

// Importez les modèles correspondant à vos tables
const UsersModel = require('./models/users.js');
const OrdersModel = require('./models/orders.js');
const OrderProductsModel = require('./models/order_products.js');
const MealsModel = require('./models/meals.js');
const SpentsModel = require('./models/spents.js');
const EarningsModel = require('./models/earnings.js');

// Définissez les modèles en utilisant sequelize.define
const Users = UsersModel(sequelize, Sequelize);
const Orders = OrdersModel(sequelize, Sequelize);
const OrderProducts = OrderProductsModel(sequelize, Sequelize);
const Meals = MealsModel(sequelize, Sequelize);
const Spents = SpentsModel(sequelize, Sequelize);
const Earnings = EarningsModel(sequelize, Sequelize);

(async () => {
    try {
        // Connexion à la base de données
        await sequelize.authenticate();
        console.log('Connexion à la base de données réussie.');

         // Synchronisation du modèle avec la base de données
        await sequelize.sync({ force: true });

        // Insérez les données dans chaque table

        // Insertion pour la table "Users"
        const hashedPassword = await bcrypt.hash('password123', 10); // Le deuxième argument (10) est le nombre de salages (salt rounds)
        await Users.create({
            email: 'john@example.com',
            first_name: 'John',
            family_name: 'Doe',
            pwd: hashedPassword,
            phone: 1234567890,
            role: 'user',
        });

        // Insertion pour la table "Orders"
        await Orders.bulkCreate([
        {
            table_number: 1,
            order_date: new Date(),
            id_waiter: 'waiter1',
            order_status: 'pending',
            price: 19.99,
        },
        // Ajoutez d'autres enregistrements pour la table "Orders" ici si nécessaire
        ]);

        // Insertion pour la table "Order_products"
        await OrderProducts.bulkCreate([
        {
            id_order: 1,
            id_meal: 1,
            quantity: 2,
        },
        // Ajoutez d'autres enregistrements pour la table "Order_products" ici si nécessaire
        ]);

        // Insertion pour la table "Meals"
        await Meals.bulkCreate([
        {
            label: 'Meal 1',
            price: 9.99,
            available: true,
        },
        {
            label: 'Meal 2',
            price: 12.99,
            available: true,
        }
        // Ajoutez d'autres enregistrements pour la table "Meals" ici si nécessaire
        ]);

        // Insertion pour la table "Spents"
        await Spents.bulkCreate([
        {
            total: 50.0,
            spent_date: new Date(),
            price: 25.0,
            title: 'Expense 1',
        },
        // Ajoutez d'autres enregistrements pour la table "Spents" ici si nécessaire
        ]);

        // Insertion pour la table "Earnings"
        await Earnings.bulkCreate([
        {
            total: 100.0,
            earning_date: new Date(),
            title: 'Earning 1',
            id_order: 1,
        },
        // Ajoutez d'autres enregistrements pour la table "Earnings" ici si nécessaire
        ]);

        console.log('Insertion des données réussie.');
    } catch (error) {
        console.error('Erreur lors de la connexion à la base de données:', error);
    } finally {
        // Fermez la connexion à la base de données
        await sequelize.close();
    }
})();
