'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Spents extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  Spents.init({
    id_spent: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    total: DataTypes.FLOAT,
    spent_date: DataTypes.DATE,
    price: DataTypes.FLOAT,
    title: DataTypes.STRING
  }, {
    sequelize,
    timestamps: false, // Ajoutez cette option pour désactiver les timestamps
    modelName: 'Spents',
  });
  return Spents;
};