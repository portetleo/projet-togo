'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Earnings extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Earnings.belongsTo(models.Orders, {
        foreignKey: {
        allowNull: false,
        name: 'order_id', // Ajouter la clé étrangère "order_id"
        },
    });
    }
  }
  Earnings.init({
    id_earning: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    total: DataTypes.FLOAT,
    earning_date: DataTypes.DATE,
    title: DataTypes.STRING,
    id_order: DataTypes.INTEGER
  }, {
    sequelize,
    timestamps: false, // Ajoutez cette option pour désactiver les timestamps
    modelName: 'Earnings',
  });
  return Earnings;
};