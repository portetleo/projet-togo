'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Order_products extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Order_products.belongsTo(models.Orders, {
            foreignKey: {
            allowNull: false,
            name: 'order_id', // Ajouter la clé étrangère "order_id"
            },
      });
      Order_products.belongsTo(models.Meals, {
        foreignKey: {
        allowNull: false,
        name: 'product_id', // Ajouter la clé étrangère "product_id
        },
    });
    }
  }
  Order_products.init({
    id_product: {
      type: DataTypes.INTEGER,
      primaryKey: true,            
      autoIncrement: true
    },
    id_order: DataTypes.INTEGER,
    id_meal: DataTypes.INTEGER,
    quantity: DataTypes.FLOAT
  }, {
    sequelize,
    timestamps: false, // Ajoutez cette option pour désactiver les timestamps
    modelName: 'Order_products',
  });
  return Order_products;
};