'use strict';
const {
    Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class Meals extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
            Meals.hasMany(models.Order_products, { foreignKey: 'product_id', as: 'order_product'});
        }
    }
    Meals.init({
        id_meal: { 
            type: DataTypes.INTEGER, 
            primaryKey: true,
            autoIncrement: true
        },
        label: DataTypes.STRING,
        price: DataTypes.FLOAT,
        available: DataTypes.BOOLEAN
    }, {
        sequelize,
        timestamps: false, // Ajoutez cette option pour désactiver les timestampss
        modelName: 'Meals',
    });
    return Meals;
};