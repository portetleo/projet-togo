'use strict';
const { Model } = require('sequelize');

module.exports = (sequelize, DataTypes) => {
    class Orders extends Model {
        static associate(models) {
        Orders.belongsTo(models.Users, {
            foreignKey: {
            allowNull: false,
            name: 'user_id', // Ajouter la clé étrangère "user_id"
            },
        });
        Orders.hasMany(models.Order_products, { foreignKey: 'order_id', as: 'order_products' });
        Orders.hasMany(models.Earnings, { foreignKey: 'order_id', as: 'earnings' });
        }
    }

    Orders.init(
        {
        id_order: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        table_number: DataTypes.INTEGER,
        order_date: DataTypes.DATE,
        id_waiter: DataTypes.STRING,
        order_status: DataTypes.STRING,
        price: DataTypes.FLOAT,
        }, {
        sequelize,
        timestamps: false, // Ajoutez cette option pour désactiver les timestamps
        modelName: 'Orders',
        }
    );

    return Orders;
};

