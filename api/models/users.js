const Orders = require('./orders');

'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Users extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Users.hasMany(models.Orders, { foreignKey: 'user_id', as: 'orders' });
    }
  }
  Users.init({
    email: {
      type: DataTypes.STRING,
      primaryKey: true,
    },
    pwd: DataTypes.STRING,
    first_name: DataTypes.STRING,
    family_name: DataTypes.STRING,
    phone: DataTypes.INTEGER,
    role: DataTypes.STRING
  }, {
    sequelize,
    timestamps: false, // Ajoutez cette option pour désactiver les timestamps
    modelName: 'Users',
  });
  return Users;
};