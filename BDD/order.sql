CREATE TABLE IF NOT EXISTS orders (
	id_order INTEGER NOT NULL PRIMARY KEY,
	tableNumber INTEGER NOT NULL,
	order_date DATE,
	id_waiter STRING NOT NULL,
	order_status STRING NOT NULL,
    price FLOAT NOT NULL,
    FOREIGN KEY (id_waiter) REFERENCES user(email),
);
