CREATE TABLE IF NOT EXISTS spents (
	id_spent INTEGER NOT NULL PRIMARY KEY,
	total FLOAT,
	spent_date DATE,
	price FLOAT,
	title STRING NOT NULL,
);
