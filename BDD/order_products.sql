CREATE TABLE IF NOT EXISTS order_products (
	id_product INTEGER NOT NULL PRIMARY KEY,
	id_order INTEGER NOT NULL,
	id_meal INTEGER NOT NULL,
	quantity FLOAT,
	FOREIGN KEY (id_order) REFERENCES orders(id_order),
	FOREIGN KEY (id_meal) REFERENCES meal(id_meal),
);
