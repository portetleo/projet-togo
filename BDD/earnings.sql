CREATE TABLE IF NOT EXISTS earnings (
	id_earning INTEGER NOT NULL PRIMARY KEY,
	total FLOAT,
	earning_date DATE,
	title STRING NOT NULL,
	id_order INTEGER NOT NULL,
	FOREIGN KEY (id_order) REFERENCES orders(id_order),
);
